import React, { Component } from 'react';

class Wrapper extends Component {
	render() {
		return (
			<div className="wrapper">
				{this.props.children}
			</div>
		);
	}
}

export class Intro extends Component {
	render() {
		return (<Wrapper>
			<div className="intro">
				<h2>A propos de moi</h2>
				<h3>Qui suis-je ?</h3>
				<p>Je m'appel Axel, jeune developpeur passioné par l'informatique depuis mes 14 ans, j'ai commence par toucher a linux, puis me suis lance dans le web par l'ancien site du zero, avant d'aller juste apres mon bac a l'ecole 42.</p>
				<h3>En details</h3>
				<p>J'aime avoir le controle total sur ce que je fais, d'ou mon passage a linux assez rapide et ma preferences pour les technologies dites "natives", pourquoi utiliser un framework JS si je peux faire la meme chose sans?</p>
				<p>Durant mon parcourt, je me suis dirige principalement dans vers l'informatique graphique, que ce soit en web, avec le CSS dans ses moindres recoins, threeJS, webGL, ou avec les projets en C de moteur de rendu, raycasting, rasterisation et raytracing a l'ecole 42. J'aime voir ce que je fais, je trouve que ca donne une certaine satisfaction !</p>
				<h3>Quel genre de developpeur je suis ?</h3>
				<p>Les objecifs que je me donne ne sont pas d'apprendre une centaine de librairies que j'utiliserais jamais mais plutot de perfectionner les racines.</p>
				<p>Si je prefere en web me cantonner au CSS pur (ou Sass/Scss), c'est parce que, contrairement a une librairie ou un framework, il est en constante amelioration depuis les annees 90 et continuera de l'etre pendant encore longtemps. C'est egalement une question de maintenabilite, c'est plus simple de debugger son propre code que celui d'un autre, surtout s'il est consequant. Et finalement, c'est aussi une question de perfomance en plus, a quoi ca sert de charger 50Mo de librairies si c'est pour au final avoir le meme resultat en moins de 200ko</p>
				<p>Evidement, l'utilisation des librairies est un gain de temps non negligable, mais je suis de ceux qui priment la qualite a la quantite ou la rapidite. Sans non plus etre dans l'exces, je prefererais utiliser ReactJS ou webGL que de les recoder maison, mais bootstrap ou jQuery n'ont pas d'interet particulier pour moi.</p>
			</div>
		</Wrapper>);
	}
}

export class Skills extends Component {
	render() {
		return (<Wrapper>
			<div className="skills">
				<h2>Languages</h2>
				<ul className="languages">
					<li className="js">JavaScript ES6</li>
					<li className="htmlcss">HTML5 / CSS3</li>
					<li className="bash">Bash Scripts</li>
					<li className="glsl">GLSL</li>
					<li className="java">Java</li>
				</ul>
				<h2>Librairies / Framework</h2>
				<ul className="librairies">
					<li className="react">ReactJS</li>
					<li className="threejs">Three.js</li>
					<li className="bootstrap">Bootstrap</li>
					<li className="jquery">jQuery</li>
					<li className="opengl">OpenGl</li>
					<li className="opencl">OpenCl</li>
					<li className="sdl">SDL 2.0</li>
					<li className="webgl">WebGL</li>
				</ul>
				<h2>Connaissances</h2>
				<ul>
					<li>Manipulation du DOM</li>
					<li>Optimisation d'une page web</li>
					<li>Mathematiques de rendu graphique</li>
					<li className="cssmiddle">Centrer en CSS</li>
					<li>Connaissances legeres en SEO</li>
					<li>Utilisation de git</li>
					<li>Installation complete d'un systeme Linux</li>
					<li>Manipulation d'un shell Linux</li>
					<li>Integration d'une maquette de designer</li>
					<li>Animations SVG (SMIL)</li>
				</ul>
			</div>
		</Wrapper>);
	}
}

export class Cursus extends Component {
	render() {
		return (<Wrapper>
			<div className="cursus">
				<h2>Etudes et formation</h2>
				<ul>
					<li>Developpeur Ecole 42 Paris</li>
					<li>Bac STI2D option SIN Lycée Clément Ader Tournan en Brie</li>
				</ul>
				<h2>Experience Professionnelle</h2>
				<ul>
					<li>C2A - Septembre 2017 - Stage (6 mois) : Java / JavaScript</li>
					<li>C2A - Mars 2018 - CDD (4 mois) : Java / JavaScript</li>
					<li>Outscale - Fevrier 2019 - CDI (3 mois) : C / Scripting</li>
					<li>Markentive - Juillet 2020 - CDI (1 an 4 mois) : Integration web</li>
				</ul>
			</div>
		</Wrapper>);
	}
}

export class Personals extends Component {
	render() {
		return (<Wrapper>
			<div className="personals">
				<h2>A propos de moi</h2>
				<p>J'adore la musique sous toutes ses formes et tous ses genres, je joue de la basse depuis bien 2 ans maintenant, egalement de la guitare et de la cornemuse</p>
				<p>Heureux proprietaire de chats, je les adore aussi sous toutes leurs formes, et c'est reciproque</p>
				<p>Recement, j'ai egalement commence la menuiserie et la lutherie</p>
				<p>Je suis quelqu'un qui a du mal a faire quelque chose que je ne comprend pas, j'ai besoin d'une raison de faire B plutot que A</p>
				<p>Tres performant sur les taches qui me plaise, je vais par contre etre plus lent sur celles qui m'ennuient. J'ai besoin d'etre mis a l'epreuve</p>
			</div>
		</Wrapper>);
	}
}
