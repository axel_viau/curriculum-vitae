import React, { Component } from 'react';

function Links(props) {
	const links = props.links;
	const res = links.map(l => <a key={l.name.toLowerCase()} href={l.link}>{l.name}</a>)
	return res;
}

export class Footer extends Component {
	render() {
		const links = [
			{name: "GitHub", link: "https://https://github.com/LunaticP"},
			{name: "LinkedIn", link: "https://www.linkedin.com/in/axel-viau-b82998187/"},
			{name: "Facebook", link: "https://www.facebook.com/axelviau"}
		];
		return (
			<footer>
				<Links links={links}/>
			</footer>
		);
	}
}
