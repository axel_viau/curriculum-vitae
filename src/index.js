import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { Header } from './header.js'
import { Footer } from './footer.js'
import { Intro } from './pages.js'
import { CSSTransitionGroup } from 'react-transition-group'

class Curriculum extends Component {
	constructor(props) {
		super(props);
		this.state = {
			page: <Intro key="intro" />
		};
	}
	handleMenu(page) {
		this.setState({page: page})
	}
	render() {
		return (
			<div className="curriculum-container">
				<Header onClick={page => this.handleMenu(page)}/>
				<div className="content-container">
					<CSSTransitionGroup
						transitionName="pChange"
						transitionEnterTimeout={300}
						transitionLeaveTimeout={300}
					>
						{this.state.page}
					</CSSTransitionGroup>
				</div>
				<Footer />
			</div>
		);
	}
}

ReactDOM.render(
	<Curriculum />,
	document.getElementById('root')
);
