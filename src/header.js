import React, { Component } from 'react';
import { Skills, Intro, Cursus, Personals } from './pages.js'

function NavButton(props) {
	return <button onClick={props.onClick}>{props.name}</button>
}

export class Header extends Component {
	render () {
		return (
			<header>
				<h1>Axel Viau</h1>
				<nav>
					<NavButton name="Intro" onClick={() => this.props.onClick(<Intro key="intro" />)} />
					<NavButton name="Skills" onClick={() => this.props.onClick(<Skills key="skills" />)} />
					<NavButton name="Cursus" onClick={() => this.props.onClick(<Cursus key="cursus" />)} />
					<NavButton name="Personals" onClick={() => this.props.onClick(<Personals key="personals" />)} />
				</nav>
			</header>
		);
	}
}
